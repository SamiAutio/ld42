﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Camera))]
[ExecuteInEditMode]
public class DithLUTImageEffect : MonoBehaviour {

	public Material material;

	public Texture LUTTexture;

	public int pixelsWidth = 200;
	public int pixelsHeight = 150;
	public bool matchCamSize = false;

	[Range(0f,0.5f)]
	public float ditherAmount = 0.1f;

	private Camera cam;

	void OnRenderImage (RenderTexture source, RenderTexture destination) {
		
		if (matchCamSize) {
			if (cam == null) cam = GetComponent<Camera>();
			pixelsWidth = cam.pixelWidth;
			pixelsHeight = cam.scaledPixelHeight;
		}

		if (LUTTexture == null) {
			Graphics.Blit(source, destination);
			return;
		}

		material.SetFloat("_PWidth", pixelsWidth);
		material.SetFloat("_PHeight", pixelsHeight);
		material.SetFloat("_DitherRange", ditherAmount);
		material.SetTexture("_LUTTex", LUTTexture);


		Graphics.Blit(source, destination, material);
	}
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Damage : MonoBehaviour {
	public GameObject target;
	private void OnTriggerEnter2D(Collider2D other) {
		if(other.GetComponent<IKillable>() != null && !other.CompareTag("Player"))
		{
			other.GetComponent<IKillable>().Kill();
			if(target != null){
				Destroy(target);
			}
		}

	}
}

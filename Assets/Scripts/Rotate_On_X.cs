﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Rotate_On_X : MonoBehaviour {
	public float rotationSpeed = 10f;
	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		transform.rotation = Quaternion.Euler(transform.rotation.eulerAngles + Vector3.forward * rotationSpeed);
	}
}

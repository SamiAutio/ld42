﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Crosshair : MonoBehaviour {
	public Transform playerTransform;
	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		Vector3 position = Camera.main.ScreenToWorldPoint(Input.mousePosition);
		position.z = 5;
		transform.position = position;
		transform.rotation = playerTransform.rotation;
	}
}

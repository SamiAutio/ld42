﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CoinScript : MonoBehaviour {
	public float upSpeed;
	// Use this for initialization
	void Start () {
		StartCoroutine(Movement());
		Destroy(gameObject, 0.75f);
	}
	
	// Update is called once per frame
	void Update () {
		
	}
	IEnumerator Movement(){
		while(transform.localScale.y >= 0){
			transform.position +=  Vector3.up * upSpeed;

			yield return null;
		}
	}
}

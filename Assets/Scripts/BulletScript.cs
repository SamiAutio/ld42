﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BulletScript : MonoBehaviour, IKillable {

	private Rigidbody2D rb2d;
	public float speed = 10f;

	// Use this for initialization
	void Start () {

	}
	// Update is called once per frame
	void Update () {
		Vector3 positionOnScreen = Camera.main.WorldToViewportPoint (transform.position);

		if(positionOnScreen.x > 1 || positionOnScreen.x < 0 || positionOnScreen.y > 1 || positionOnScreen.y < 0){
			Destroy(gameObject);
		}
	}
	private void OnTriggerEnter2D(Collider2D other) {
		if(other.CompareTag("Enemy")){
			Destroy(gameObject);
		}
	}
	public void Kill(){
		Destroy(gameObject);
	}
}

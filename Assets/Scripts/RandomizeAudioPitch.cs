﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(AudioSource))]
public class RandomizeAudioPitch : MonoBehaviour {
	public float minPitch = 0.75f;
	public float maxPitch = 1.25f;
	public bool randomizeVolume;
	public float minVolume = 0.5f;
	public float maxVolume = 1f;
	public AudioSource[] audioSource;
	// Use this for initialization
	void Start () {
		foreach(AudioSource a in audioSource){
			float pitch = Random.Range(minPitch, maxPitch);
			
			a.pitch = pitch;
			if(randomizeVolume){
				a.volume = Random.Range(minPitch, maxPitch);
			}
		}
	}
	
	// Update is called once per frame
	void Update () {
		
	}
}

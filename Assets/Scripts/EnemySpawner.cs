﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemySpawner : MonoBehaviour {

	public int maxEnemies;

	public int[] levels;

	public int enemiesKilled;

	public float maxSpawnTime;
	public float minSpawnTime;
	public float minReduction;
	public float maxReduction;
	public GameObject[] enemies;
	public bool spawning;

	private List<GameObject> enemyPool1;
	private List<GameObject> enemyPool2;
	private List<GameObject> enemyPool3;
	private List<GameObject> enemyPool4;
	private Vector3[] spawns;


	// Use this for initialization
	void Start () {
		spawns = new [] {	new Vector3(-0.2f, 0.33f, 0f),
							new Vector3(-0.2f, 0.66f, 0f),
							new Vector3(-0.2f, 0.99f, 0f),
							new Vector3(1.1f, 0.33f, 0f),
							new Vector3(1.1f, 0.66f, 0f),
							new Vector3(1.1f, 0.99f, 0f),
							new Vector3(0.33f, -0.2f, 0f),
							new Vector3(0.66f, -0.2f, 0f),
							new Vector3(0.99f, -0.2f, 0f),
							new Vector3(0.33f, 1.1f, 0f),
							new Vector3(0.66f, 1.1f, 0f),
							new Vector3(0.99f, 1.1f, 0f)
						};

		Vector3 pos = new Vector3(1.1f, Random.value, 0f);
		pos = Camera.main.ViewportToWorldPoint(pos);

		enemyPool1 = new List<GameObject>();
		enemyPool2 = new List<GameObject>();
		enemyPool3 = new List<GameObject>();
		enemyPool4 = new List<GameObject>();

		for(int i = 0; i < maxEnemies; i++){
			GameObject obj = Instantiate(enemies[0]);
			obj.SetActive(false);
			enemyPool1.Add(obj);
		}

		for(int i = 0; i < maxEnemies; i++){
			GameObject obj = Instantiate(enemies[1]);
			obj.SetActive(false);
			enemyPool2.Add(obj);
		}

		for(int i = 0; i < maxEnemies; i++){
			GameObject obj = Instantiate(enemies[2]);
			obj.SetActive(false);
			enemyPool3.Add(obj);
		}
		for(int i = 0; i < 2; i++){
			GameObject obj = Instantiate(enemies[3]);
			obj.SetActive(false);
			enemyPool4.Add(obj);
		}

		StartCoroutine(SpawnEnemies());
	}
	
	// Update is called once per frame
	void Update () {

	}

	IEnumerator SpawnEnemies(){
		while(spawning){
			int maxSelection = 0;
			if(enemiesKilled >= levels[4]){
				maxSelection = 5;
			} else if(enemiesKilled >= levels[3]){
				maxSelection = 4;
			} else if(enemiesKilled >= levels[2]){
				maxSelection = 3;
			} else if(enemiesKilled >= levels[1]){
				maxSelection = 2;
			} else if(enemiesKilled >= levels[0]){
				maxSelection = 1;
			}

			int selection = Random.Range(0,maxSelection);

			if(selection == 0){
				for(int i = 0; i < enemyPool1.Count; i++){
					if(!enemyPool1[i].activeInHierarchy){
						
						Vector3 pos = spawns[Random.Range(0,spawns.Length)];

						pos = Camera.main.ViewportToWorldPoint(pos);
						pos.z = 0f;

						enemyPool1[i].transform.position = pos;
						enemyPool1[i].SetActive(true);
						break;
					}
				}
			} else if(selection == 1 && enemiesKilled > levels[0]){
				for(int i = 0; i < enemyPool2.Count; i++){
					if(!enemyPool2[i].activeInHierarchy){
						
						Vector3 pos = spawns[Random.Range(0,spawns.Length)];

						pos = Camera.main.ViewportToWorldPoint(pos);
						pos.z = 0f;

						enemyPool2[i].transform.position = pos;
						enemyPool2[i].SetActive(true);
						break;
					}
				}
			} else if(selection == 2 && enemiesKilled > levels[1]){
				for(int i = 0; i < enemyPool3.Count; i++){
					if(!enemyPool3[i].activeInHierarchy){
						
						Vector3 pos = spawns[Random.Range(0,spawns.Length)];

						pos = Camera.main.ViewportToWorldPoint(pos);
						pos.z = 0f;

						enemyPool3[i].transform.position = pos;
						enemyPool3[i].SetActive(true);
						break;
					}
				}
			}

			selection = Random.Range(0,100);

			if(selection == 3 && enemiesKilled > levels[4]){
				for(int i = 0; i < enemyPool4.Count; i++){
					if(!enemyPool4[i].activeInHierarchy){
						
						Vector3 pos = spawns[Random.Range(0,spawns.Length)];

						pos = Camera.main.ViewportToWorldPoint(pos);
						pos.z = 0f;

						enemyPool4[i].transform.position = pos;
						enemyPool4[i].SetActive(true);
						break;
					}
				}
			}

			yield return new WaitForSeconds(Random.Range(minSpawnTime,maxSpawnTime));

		}
	}

	public void KilledEnemy(){
			enemiesKilled++;

			if (enemiesKilled > levels[5] && enemiesKilled % 12 == 0){
				SubSpawnTimes(minReduction, maxReduction);
			} else if (enemiesKilled == levels[5]){
				SubSpawnTimes(minReduction, maxReduction);
			} else if(enemiesKilled == levels[4]){
				SubSpawnTimes(minReduction, maxReduction);
			} else if(enemiesKilled == levels[3]){
				SubSpawnTimes(minReduction, maxReduction);
			} else if(enemiesKilled == levels[2]){
				SubSpawnTimes(minReduction, maxReduction);
			} else if(enemiesKilled == levels[1]){
				SubSpawnTimes(minReduction, maxReduction);
			}else if(enemiesKilled == levels[0]){
				SubSpawnTimes(minReduction, maxReduction);
			} 
	}
	public void SubSpawnTimes(float min, float max){
		if(min < minSpawnTime){
			minSpawnTime -= min;
		}
		if(max < maxSpawnTime){
			maxSpawnTime -= max;
		}
	}
}

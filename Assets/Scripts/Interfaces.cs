﻿public interface IKillable{
	void Kill();
}
public interface IDamage{
	void Damage(int damage);
}
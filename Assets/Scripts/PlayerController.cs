﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayerController : MonoBehaviour {

	public Animator animator;
	private GameObject projectile;
	public GameObject[] projectiles;
	public Text looperText;
	public Text gunText;
	public Text minesText;
	public float bulletSpeed;
	public float bulletReach;
	public float bulletTimer = 1f;
	public float speed = 1f;
	public float lookSpeed = 0.01f;
	public bool looper = false;
	public bool mines = false;
	private bool shooting = false;
	private float oldLookAngle;
	private float lookAngle;
	
	private AudioSource audioSource;
	private AudioClip audioClip;

	// Use this for initialization
	void Start () {
		audioSource = gameObject.GetComponent<AudioSource>();
		projectile = projectiles[0];

		gunText.color = new Color32(255,255,255,255);
		looperText.color = new Color32(128,128,128,128);
		minesText.color = new Color32(128,128,128,128);

	}
	
	// Update is called once per frame
	void Update () {
		if(Time.timeScale != 0f){
			if(Sinput.GetButton("Fire1")){
				if(!shooting){
					shooting = true;
					StartCoroutine(Attack());
				}
			}
			
			// Move the player
			transform.position += Sinput.GetAxis("Horizontal") * speed * Vector3.right * Time.deltaTime;
			transform.position += Sinput.GetAxis("Vertical") * speed * Vector3.up * Time.deltaTime;

			// Limit player's movement to the screen
			Vector3 positionOnScreen = Camera.main.WorldToViewportPoint (transform.position);
			
			positionOnScreen.x = Mathf.Clamp01(positionOnScreen.x);
			positionOnScreen.y = Mathf.Clamp01(positionOnScreen.y);

			transform.position = Camera.main.ViewportToWorldPoint(positionOnScreen);

			// Check if controller is connected

			float oldLookAngle = lookAngle;

			lookAngle = -Mathf.Atan2(Sinput.GetAxis("Look Vertical"), -Sinput.GetAxis("Look Horizontal")) * Mathf.Rad2Deg;

			//lookAngle = Mathf.Lerp(oldLookAngle, lookAngle, lookSpeed);

			if(true ){//Input.GetJoystickNames().Length > 0
				if(lookAngle != -180){
					//transform.rotation = Quaternion.Lerp(transform.rotation, Quaternion.Euler(transform.eulerAngles.x , transform.eulerAngles.y, lookAngle), lookSpeed * Time.deltaTime);
					//transform.eulerAngles =  new Vector3(transform.eulerAngles.x , transform.eulerAngles.y, lookAngle);
				}
			}
			
			Vector2 mousePos = Input.mousePosition;

			positionOnScreen = Camera.main.ScreenToWorldPoint (new Vector3(mousePos.x, mousePos.y, transform.position.z - Camera.main.transform.position.z));


			Vector2 targetDir = Camera.main.ScreenToViewportPoint(mousePos);
			float angle = AngleBetweenTwoPoints(positionOnScreen, targetDir);
			transform.rotation =  Quaternion.Euler (new Vector3(0f,0f,Mathf.Atan2((positionOnScreen.y - transform.position.y), (positionOnScreen.x - transform.position.x))*Mathf.Rad2Deg -180));
			



			if(Input.GetKeyDown("1")){
				projectile = projectiles[0];
				gunText.color = new Color32(255,255,255,255);
				looperText.color = new Color32(128,128,128,128);
				minesText.color = new Color32(128,128,128,128);

			} else if (Input.GetKeyDown("2") && looper){
				projectile = projectiles[1];
				gunText.color = new Color32(128,128,128,128);
				looperText.color = new Color32(255,255,255,255);
				minesText.color = new Color32(128,128,128,128);

			} else if (Input.GetKeyDown("3") && mines){
				projectile = projectiles[2];
				gunText.color = new Color32(128,128,128,128);
				looperText.color = new Color32(128,128,128,128);
				minesText.color = new Color32(255,255,255,255);
			}

			//Vector2 newDirection = Vector3.RotateTowards(transform.forward, targetDir, Time.deltaTime, 0.0f);

			//transform.LookAt(targetDir); //= Quaternion.LookRotation(targetDir);

		}
	}
	float AngleBetweenTwoPoints(Vector3 a, Vector3 b) {
         return Mathf.Atan2(a.y - b.y, a.x - b.x) * Mathf.Rad2Deg;
     }

	IEnumerator Attack(){
		while(shooting){

			GameObject bullet = Instantiate(projectile, transform.position, Quaternion.identity) as GameObject;
			audioSource.Play();
			Vector3 bulletTarget = Input.mousePosition;

			bulletTarget.z = 0f;
			if(bullet.GetComponent<Rigidbody2D>() != null && bullet.GetComponent<BulletScript>() != null){
				bullet.GetComponent<Rigidbody2D>().velocity = -transform.right * bullet.GetComponent<BulletScript>().speed;
			}

			bullet.transform.localScale = new Vector3(bulletReach, bulletReach, 1f);
			
			animator.SetTrigger("Attack");

			yield return new WaitForSeconds(bulletTimer);
			
			shooting = false;

		}
		
	}

	public void AddSpeed(float toAdd){
		if(toAdd < bulletTimer){
			bulletTimer -= toAdd;
		}
	}

	public void AddReach(float toAdd){
		bulletReach += toAdd;
	}

	public void AddLooper(){
		looper = true;
	}
	public void AddMines(){
		mines = true;
	}
}

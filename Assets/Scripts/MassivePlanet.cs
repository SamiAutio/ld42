﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MassivePlanet : EnemyPlanet {
	public int health;
	private AudioSource audioSource;
	// Use this for initialization
	void Start () {
		if(GameObject.Find("Earth") != null){
			playerTransform = GameObject.Find("Earth").GetComponent<Transform>();
			playerPosition = playerTransform.position;
		}
		if(GameObject.Find("GameManager") != null){
			gm = GameObject.Find("GameManager").GetComponent<GameManager>();
		}
		audioSource = gameObject.GetComponent<AudioSource>();
	}
	
	// Update is called once per frame
	void Update () {
		Movement();
	}
	public override void Kill(){
		health--;
		audioSource.Play();
		if(health <= 0){
			if(deathEffect != null){
				GameObject death = Instantiate(deathEffect, transform.position, Quaternion.identity);
			}
			if(coin != null){
				GameObject death = Instantiate(coin, transform.position, Quaternion.identity);
			}
			
			gm.AddMoney(money);

			gameObject.SetActive(false);
		}
	}
	public override void Movement(){
		if(playerTransform != null){
			playerPosition = playerTransform.position;
		}
		else
		{
			
			playerPosition =  Camera.main.ViewportToWorldPoint(new Vector3(Random.Range(-0.1f, 1.1f),Random.Range(-0.1f, 1.1f), 0f));
			playerPosition.z = 0f;
		}
		transform.position = Vector3.MoveTowards(transform.position, playerPosition, Time.deltaTime * speed);
		transform.Rotate(Vector3.right * Time.deltaTime);
	}
}

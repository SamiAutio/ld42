﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class GameManager : MonoBehaviour {

	public int playerMoney = 0;
	private int playerScore;
	public Text moneyText;
	public Text scoreText;
	public Button[] store;
	public int[] prices;
	public GameObject pauseMenu;
	public GameObject gameOverPanel;
	public GameObject storePanel;
	public PlayerController player;
	public EnemySpawner enemySpawner;
	public Death playerDeath;

	// Use this for initialization
	void Start () {
		Time.timeScale = 1f;
		Cursor.visible = true;
		Cursor.lockState = CursorLockMode.Confined;
	}
	
	// Update is called once per frame
	void Update () {
		if(playerDeath.dead){
			gameOverPanel.SetActive(true);
			Time.timeScale = 0f;
			// Cursor.visible = true;
			Cursor.lockState = CursorLockMode.None;
		}
		if(Sinput.GetButtonDown("Pause")){
			if(storePanel.activeInHierarchy){
				storePanel.SetActive(false);
				Time.timeScale = 1f;
				Cursor.lockState = CursorLockMode.Confined;
			} else {
				Pause();
			}
		}
		if(Sinput.GetButtonDown("Buy")){
			if(!storePanel.activeInHierarchy){
				storePanel.SetActive(true);
				Time.timeScale = 0f;
				// Cursor.visible = true;
				Cursor.lockState = CursorLockMode.None;
			} else {
				storePanel.SetActive(false);
				Time.timeScale = 1f;
				// Cursor.visible = false;
				Cursor.lockState = CursorLockMode.Confined;
			}
			

		}
		for(int i = 0; i < prices.Length; i++){
			if(playerMoney >= prices[i]){
				store[i].interactable = true;
			}
		}
	}

	public void Pause(){
		if(!playerDeath.dead){
			if(Time.timeScale == 0f){
				Time.timeScale = 1f;
				pauseMenu.SetActive(false);
				// Cursor.visible = false;
				Cursor.lockState = CursorLockMode.Confined;
			} else {
				Time.timeScale = 0f;
				pauseMenu.SetActive(true);
				// Cursor.visible = true;
				Cursor.lockState = CursorLockMode.None;
			}
		}
	}

	public void AddMoney(int money){
		playerScore = playerMoney += money;
		enemySpawner.KilledEnemy();
		moneyText.text = playerMoney.ToString();
		scoreText.text = "You earned: " + enemySpawner.enemiesKilled.ToString() + " coins!";
	}

	public bool RemoveMoney(int money){
		if(money <= playerMoney){
			playerMoney -= money;
			moneyText.text = playerMoney.ToString();
			return true;
		} else {
			return false;
		}
	}
	public void AddSpeed(float toAdd){
		if(player != null && RemoveMoney(prices[0])){
			player.AddSpeed(toAdd);
		}
	}

	public void AddReach(float toAdd){
		if(player != null && RemoveMoney(prices[1])){
			player.AddReach(toAdd);
		}
	}
	public void AddCannon(){
		if(player != null && !player.looper && RemoveMoney(prices[2])){
			player.AddLooper();
		}
	}
	public void AddMines(){
		if(player != null && !player.mines && RemoveMoney(prices[3])){
			player.AddMines();
		}
	}
	public void Restart(){
		//Pause();
		SceneManager.LoadSceneAsync(SceneManager.GetActiveScene().buildIndex, LoadSceneMode.Single);
	}
	
	public void QuitGame(){
		Application.Quit();
	}
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DryPlanet : EnemyPlanet
{
    public int movementTime;
    private bool moving = false;
    public override void Movement(){
        if(!moving){
            moving = true;
            if(playerTransform != null){
                playerPosition = playerTransform.position;
            }
            else
            {
                
                playerPosition =  Camera.main.ViewportToWorldPoint(new Vector3(Random.Range(-0.1f, 1.1f),Random.Range(-0.1f, 1.1f), 0f));
                playerPosition.z = 0f;
            }
            
            StartCoroutine(Move());
        }
    }

    IEnumerator Move(){
        int tempMovementTime = movementTime;
        while(moving){
            transform.position = Vector3.MoveTowards(transform.position, playerPosition, Time.deltaTime * speed);
            transform.Rotate(Vector3.right * Time.deltaTime);

            yield return null;

            tempMovementTime--;
            if(tempMovementTime <= 0){
                moving = false;
                tempMovementTime = movementTime;
            }

        }

        
    }
}

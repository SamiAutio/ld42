﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Death : MonoBehaviour, IKillable {
	public GameObject target;
	public GameObject deathEffect;
	public bool dead = false;
	public float deathDelay = 1f;
	private void OnTriggerEnter2D(Collider2D other) {

		if(other.GetComponent<IDamage>() != null && !other.CompareTag("Bullet")){
			StartCoroutine(DelayedDeath());
		}
	}

	IEnumerator DelayedDeath(){
		dead = true;
		yield return new WaitForSeconds(deathDelay);

		Kill();
	}
	
	public void Kill(){
		if(deathEffect != null){
			GameObject death = Instantiate(deathEffect, transform.position, Quaternion.identity);
		}
		target.SetActive(false);
	}
}

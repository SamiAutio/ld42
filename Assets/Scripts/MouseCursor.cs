﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MouseCursor : MonoBehaviour {

	public Texture2D cursorTexture;
	

	// Use this for initialization
	void Start () {
		Cursor.visible = true;
		Cursor.SetCursor(cursorTexture, new Vector2(0f,0f), CursorMode.Auto);
	}
	
	// Update is called once per frame
	void Update () {
		// Vector3 tempPosition = Camera.main.ScreenToWorldPoint(Input.mousePosition);
	
		// tempPosition.z = 0;
		// transform.position = tempPosition;
	}
}

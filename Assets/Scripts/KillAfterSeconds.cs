﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class KillAfterSeconds : MonoBehaviour {
	public float deathTime = 1f;
	// Use this for initialization
	void Start () {
		Destroy(gameObject, deathTime);
	}
}
